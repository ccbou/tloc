//import Phaser from 'phaser';
//import TextPlayer from "../texts/playerText";

export default class Player {
  constructor(scene, x, y) {
    this.scene = scene;

    const anims = scene.anims;
    // down 0-3
    // right 4-7
    // up 8-11
    anims.create({
      key: "down",
      frames: anims.generateFrameNumbers("player", { start: 0, end: 3 }),
      frameRate: 8,
      repeat: -1,
    });

    anims.create({
      key: "right",
      frames: anims.generateFrameNumbers("player", { start: 4, end: 7 }),
      frameRate: 8,
      repeat: -1,
    });

    anims.create({
      key: "up",
      frames: anims.generateFrameNumbers("player", { start: 8, end: 11 }),
      frameRate: 8,
      repeat: -1,
    });

    this.sprite = scene.matter.add
      .sprite(x, y, "player", 0)
      .setScale(1.3)
      .setFixedRotation()
      // .setSize(30, 40)
      // .setOffset(0, 24)
      ;

    this.keys = scene.input.keyboard.createCursorKeys();
  }

  update() {
    const keys = this.keys;
    const sprite = this.sprite;
    const speed = 7;

    // Horizontal movement
    if (keys.left.isDown) {
      sprite.setVelocityX(-speed);
      sprite.setFlipX(true);
    } else if (keys.right.isDown) {
      sprite.setVelocityX(speed);
      sprite.setFlipX(false);
    } else {
      sprite.setVelocityX(0);
    }

    // Vertical movement
    if (keys.up.isDown) {
      sprite.setVelocityY(-speed);
    } else if (keys.down.isDown) {
      sprite.setVelocityY(speed);
    } else {
      sprite.setVelocityY(0);
    }

    // Update the animation last and give left/right animations precedence over up/down animations
    if (keys.left.isDown || keys.right.isDown) {
      sprite.anims.play("right", true);
    } else if (keys.down.isDown) {
      sprite.anims.play("down", true)
    } else if (keys.up.isDown) {
      sprite.anims.play("up", true);
    } else {
      sprite.anims.stop();

      // // If we were moving, pick and idle frame to use
      // if (prevVelocity.x < 0) sprite.setTexture("player", "right");
      // else if (prevVelocity.x > 0) sprite.setTexture("player", "right");
      // else if (prevVelocity.y < 0) sprite.setTexture("player", "up");
      // else if (prevVelocity.y > 0) sprite.setTexture("player", "down");
    }
  }
}



//   destroy() {
//     this.sprite.destroy();
//   }
// }
