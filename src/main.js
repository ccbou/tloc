// @flow

import Phaser from "phaser";
import UIPlugin from "phaser3-rex-plugins/templates/ui/ui-plugin.js";
//import WorldScene from "./scenes/worldScene";
import TextPlayer from "./texts/playerText";
import WorldScene from "./scenes/worldScene";
//import TextPlayer from "./texts/playerText";

export const config = {
	type: Phaser.AUTO,
	//backgroundColor: "#125555",
	// width: 800,
	// height: 600,
	width: window.innerWidth,
	height: window.innerHeight,
	parent: "game",
	scene: WorldScene,
	scale: {
		mode: Phaser.Scale.FIT,
		// 	// parent: "game",
		// 	width: window.innerWidth,
		// 	height: window.innerHeight,
	},
	physics: {
		default: "matter",
		matter: {
			gravity: {
				x: 0,
				y: 0
			}
		}
	},
	plugins: {
		scene: [{
			key: 'rexUI',
			plugin: UIPlugin,
			mapping: 'rexUI'
		}]
	}
};

export class Game extends Phaser.Game {
	constructor(config) {
		super(config);
	}
}

window.onload = () => {
	const game = new Phaser.Game(config);
	game.scale.setGameSize(window.innerWidth, window.innerHeight);
	//   resizeGame();


};
