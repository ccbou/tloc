import Phaser from 'phaser';
// import { TextBox } from 'phaser3-rex-plugins/templates/ui/ui-components';
import TextBox from 'phaser3-rex-plugins/templates/ui/textbox/TextBox';
import { config } from '../main';


const COLOR_PRIMARY = 0x4e342e;
const COLOR_LIGHT = 0x7b5e57;
const COLOR_DARK = 0x260e04;

let content = 'test';

// export default class TextPlayer extends TextBox {
//     constructor(scene, config) {
//         super(scene, config);

//         scene.add.existing(this);
//     }
// }

// export default class TextPlayer extends TextBox {
//     constructor(scene) {
//         super(scene, config)
//         scene.add.existing(this);
//     }

//     create() {
//         createTextBox(this, 100, 100, {
//             wrapWidth: 500,
//         })
//             .start(content, 50);
//     }
// }

// const GetValue = Phaser.Utils.Objects.GetValue;
// let createTextBox = function (scene, x, y, config) {
//     let wrapWidth = GetValue(config, 'wrapWidth', 0);
//     let fixedWidth = GetValue(config, 'fixedWidth', 0);
//     let fixedHeight = GetValue(config, 'fixedHeight', 0);
//     let textBox = scene.rexUI.add.textBox({
//         x: x,
//         y: y,

//         background: scene.rexUI.add.roundRectangle(0, 0, 2, 2, 20, COLOR_PRIMARY).setStrokeStyle(2, COLOR_LIGHT),
//         text: getBuiltInText(scene, wrapWidth, fixedWidth, fixedHeight),
//         space: {
//             left: 20,
//             right: 20,
//             top: 20,
//             bottom: 20,
//             icon: 10,
//             text: 10,
//         }
//     })
//         .setOrigin(0)
//         .layout();
// }

// let getBuiltInText = function (scene, wrapWidth, fixedWidth, fixedHeight) {
//     return scene.add.text(0, 0, '', {
//         fontSize: '20px',
//         wordWrap: {
//             width: wrapWidth
//         },
//         maxLines: 3
//     })
//         .setFixedSize(fixedWidth, fixedHeight);
// }

export default class TextPlayer extends TextBox {
    constructor(scene) {
        super(scene, config)
        // scene.add.existing(this);
        this.scene = scene;
        // this.config = config;
    }

    create() {
        createTextBox(this, 100, 100, {
            wrapWidth: 500,
        })
            .start(content, 50);

        createTextBox(this, 100, 400, {
            wrapWidth: 500,
            fixedWidth: 500,
            fixedHeight: 65,
        })
            .start(content, 50);
    }

    update() { }
}


const GetValue = Phaser.Utils.Objects.GetValue;
var createTextBox = function (scene, x, y, config) {
    var wrapWidth = GetValue(config, 'wrapWidth', 0);
    var fixedWidth = GetValue(config, 'fixedWidth', 0);
    var fixedHeight = GetValue(config, 'fixedHeight', 0);
    var textBox = scene.rexUI.add.textBox({
        x: x,
        y: y,

        background: scene.rexUI.add.roundRectangle(0, 0, 2, 2, 20, COLOR_PRIMARY)
            .setStrokeStyle(2, COLOR_LIGHT),

        icon: scene.rexUI.add.roundRectangle(0, 0, 2, 2, 20, COLOR_DARK),

        // text: getBuiltInText(scene, wrapWidth, fixedWidth, fixedHeight),
        text: getBBcodeText(scene, wrapWidth, fixedWidth, fixedHeight),

        action: scene.add.image(0, 0, 'nextPage').setTint(COLOR_LIGHT).setVisible(false),

        space: {
            left: 20,
            right: 20,
            top: 20,
            bottom: 20,
            icon: 10,
            text: 10,
        }
    })
        .setOrigin(0)
        .layout();

    textBox
        .setInteractive()
        .on('pointerdown', function () {
            var icon = this.getElement('action').setVisible(false);
            this.resetChildVisibleState(icon);
            if (this.isTyping) {
                this.stop(true);
            } else {
                this.typeNextPage();
            }
        }, textBox)
        .on('pageend', function () {
            if (this.isLastPage) {
                return;
            }

            var icon = this.getElement('action').setVisible(true);
            this.resetChildVisibleState(icon);
            icon.y -= 30;
            var tween = scene.tweens.add({
                targets: icon,
                y: '+=30', // '+=100'
                ease: 'Bounce', // 'Cubic', 'Elastic', 'Bounce', 'Back'
                duration: 500,
                repeat: 0, // -1: infinity
                yoyo: false
            });
        }, textBox)
    //.on('type', function () {
    //})

    return textBox;
}

var getBuiltInText = function (scene, wrapWidth, fixedWidth, fixedHeight) {
    return scene.add.text(0, 0, '', {
        fontSize: '20px',
        wordWrap: {
            width: wrapWidth
        },
        maxLines: 3
    })
        .setFixedSize(fixedWidth, fixedHeight);
}

var getBBcodeText = function (scene, wrapWidth, fixedWidth, fixedHeight) {
    return scene.rexUI.add.BBCodeText(0, 0, '', {
        fixedWidth: fixedWidth,
        fixedHeight: fixedHeight,

        fontSize: '20px',
        wrap: {
            mode: 'word',
            width: wrapWidth
        },
        maxLines: 3
    })
}