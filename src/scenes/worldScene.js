import Phaser from "phaser";
import Player from "../characters/player";
import { TextBox } from 'phaser3-rex-plugins/templates/ui/ui-components.js';
import TextPlayer from "../texts/playerText";
import { config } from "../main";
// import  World  from "matter";


export default class WorldScene extends Phaser.Scene {
  constructor() {
    super("worldScene");
    this.controls = undefined;
    this.player = undefined;
  }

  preload() {
    this.load.image("tiles", "assets/tilesets/tuxmon-sample-32px-extruded.png");
    this.load.tilemapTiledJSON("map", "assets/tilemaps/world.json");
    this.load.spritesheet("player", "assets/spritesheets/player.png", {
      frameWidth: 18,
      frameHeight: 24,
      margin: 1,
      spacing: 3
    });
  }

  create() {
    //const map = this.make.tilemap({ key: "map" });
    const map = this.createMap();

    const tileset = map.addTilesetImage("world-tileset", "tiles");

    const ground = map.createStaticLayer("Ground", tileset, 0, 0);
    const worldLayer = map.createDynamicLayer("World", tileset, 0, 0);
    const above = map.createStaticLayer("Above", tileset, 0, 0);


    map.findObject("Objects", function (object) {
      if (object.name === "Spawn") {
        this.player = new Player(this, object.x, object.y);
      }
    }, this);

    // console.log(this.player.scene.x);

    // this.label = this.add.text(this.object.x, player.y, 'Test');

    // var textBox = new TextPlayer(this.scene);
    // this.player.appendText(textBox);

    // let textBox = new TextPlayer(this);
    // textBox.setText()
    // textBox.start('text', 3000);
    // textBox.create();
    // this.add.existing(textBox);
    // console.log(textBox);

    this.matter.world.setBounds(0, 0, map.widthInPixels, map.heightInPixels);

    worldLayer.setCollisionByProperty({ collides: true });
    above.setDepth(10);

    this.matter.world.convertTilemapLayer(worldLayer);

    //this.matter.add.collider(this.player.sprite, world);

    const camera = this.cameras.main;
    camera.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
    camera.startFollow(this.player.sprite);

    // this.matter.world.createDebugGraphic();
    // this.matter.world.drawDebug = false;
    // this.input.keyboard.on("keydown_D", event => {
    //   this.matter.world.drawDebug = !this.matter.world.drawDebug;
    //   this.matter.world.debugGraphic.clear();
    // });

    // Debug graphics
    this.input.keyboard.once("keydown_D", event => {
      // Turn on matter debugging to show player's hitbox
      this.matter.world.createDebugGraphic();

      // Create worldLayer collision graphic above the player, but below the help text
      const graphics = this.add
        .graphics()
        .setAlpha(0.75)
        .setDepth(20);
      worldLayer.renderDebug(graphics, {
        tileColor: null, // Color of non-colliding tiles
        collidingTileColor: new Phaser.Display.Color(243, 134, 48, 255), // Color of colliding tiles
        faceColor: new Phaser.Display.Color(40, 39, 37, 255) // Color of colliding face edges
      });
    });
  }

  update(time, delta) {
    // Apply the controls to the camera each update tick of the game
    // this.controls.update(delta);
    this.player.update();
  }

  createMap() {
    const map = this.make.tilemap({ key: "map" });

    return map;
  }
}
